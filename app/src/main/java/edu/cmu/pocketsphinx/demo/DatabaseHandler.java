package edu.cmu.pocketsphinx.demo;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by Victor on 2017-01-28.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "CuesDatabase";

    // Cues table name
    private static String TABLE_CUES = "TABLE_CUES";

    // Cues Table Columns names
    private static String MAIN_KEY = "MAIN_KEY";
    private static String FIELD_TIME = "FIELD_TIME";
    private static String FIELD_LOCATION = "FIELD_LOCATION";
    private static String FIELD_EMSTATE = "FIELD_EMSTATE";
    private static String FIELD_OTHERS = "FIELD_OTHERS";
    private static String FIELD_ACTION = "FIELD_ACTION";
    private static String FIELD_NOTES = "FIELD_NOTES";
    private static String FIELD_DATE = "FIELD_DATE";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_CUES_TABLE = "CREATE TABLE " + TABLE_CUES + "("
                + MAIN_KEY + " INTEGER PRIMARY KEY," + FIELD_TIME + " TEXT," + FIELD_LOCATION + " TEXT," + FIELD_EMSTATE + " TEXT," + FIELD_OTHERS + " TEXT," + FIELD_ACTION + " TEXT," + FIELD_NOTES + " TEXT," + FIELD_DATE + " INTEGER" + ")";
        db.execSQL(CREATE_CUES_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUES);

        // Create tables again
        onCreate(db);
    }

    public void addHabitCue(HabitCue _cue)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIELD_TIME, _cue.getTime()); // Cue Time
        values.put(FIELD_LOCATION, _cue.getLocation()); // Cue Location
        values.put(FIELD_EMSTATE, _cue.getEmstate()); // Cue State
        values.put(FIELD_OTHERS, _cue.getOthers()); // Cue Others
        values.put(FIELD_ACTION, _cue.getAction()); // Cue Action
        values.put(FIELD_DATE, new Date().getTime()); // Cue creation Datetime

        // Inserting Row
        db.insert(TABLE_CUES, null, values);
        db.close(); // Closing database connection
    }

    public HabitCue getHabitCue(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CUES, new String[] { MAIN_KEY,
                        FIELD_TIME, FIELD_LOCATION,  FIELD_EMSTATE, FIELD_OTHERS, FIELD_ACTION, FIELD_DATE}, MAIN_KEY + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        else
            return new HabitCue(-1, "NULL", "NULL", "NULL", "NULL", "NULL");

        HabitCue cue = new HabitCue(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
        cue.setDatetime(Long.parseLong(cursor.getString(6)));
        return cue;
    }

    public int updateHabitCue (HabitCue _cue) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MAIN_KEY, _cue.getId()); // Cue Id
        values.put(FIELD_TIME, _cue.getTime()); // Cue Time
        values.put(FIELD_LOCATION, _cue.getLocation()); // Cue Location
        values.put(FIELD_EMSTATE, _cue.getEmstate()); // Cue State
        values.put(FIELD_OTHERS, _cue.getOthers()); // Cue Others
        values.put(FIELD_ACTION, _cue.getAction()); // Cue Action
        values.put(FIELD_DATE, _cue.getDatetime());

        // updating row
        return db.update(TABLE_CUES, values, MAIN_KEY + " = ?",
                new String[] { String.valueOf(_cue.getId()) });
    }

    public void deleteHabitCue(HabitCue _cue) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CUES, MAIN_KEY + " = ?",
                new String[] { String.valueOf(_cue.getId()) });
        db.close();
    }

    public ArrayList<HabitCue> queryHabitCue(long _startTime, long _endTime)
    {
        ArrayList<HabitCue> ret_list = new ArrayList<HabitCue>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selection = "SELECT * FROM " + TABLE_CUES + " WHERE " + FIELD_DATE + " >= ? AND " + FIELD_DATE + " <= ?";

        String[] args = {String.valueOf(_startTime), String.valueOf(_endTime)};

        Cursor cursor = db.rawQuery(selection, args);

        try {
            while (cursor.moveToNext()) {
                // Each row
                HabitCue cue = new HabitCue(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
                cue.setDatetime(Long.parseLong(cursor.getString(6)));
                ret_list.add(cue);
            }
        } finally {
            cursor.close();
        }
        return ret_list;
    }

    public int CountFilteredList(ArrayList<HabitCue> lst, String filterAttr, String Keepers)
    {
        int total = 0;
        for (int cnt = 0; cnt < lst.size(); cnt++)
        {
            switch (filterAttr)
            {
                case "FIELD_TIME":
                {
                    if (lst.get(cnt).getTime() == Keepers)
                        total++;
                }
                case "FIELD_LOCATION":
                {
                    if (lst.get(cnt).getLocation() == Keepers)
                        total++;
                }
                case "FIELD_EMSTATE":
                {
                    if (lst.get(cnt).getEmstate() == Keepers)
                        total++;
                }
                case "FIELD_OTHERS":
                {
                    if (lst.get(cnt).getOthers() == Keepers)
                        total++;
                }
                case "FIELD_ACTION":
                {
                    if (lst.get(cnt).getAction() == Keepers)
                        total++;
                }
            }
        }
        return total;
    }
}
