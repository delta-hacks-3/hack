package edu.cmu.pocketsphinx.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;


public class ExampleGraph extends AppCompatActivity {

    BarChart barChart;
    ArrayList<String> theDates;
    ArrayList<BarEntry> barEntries;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_graph);


        barChart = (BarChart) findViewById(R.id.bargraph);

        barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(0f,20f));
        barEntries.add(new BarEntry(2f,30f));
        barEntries.add(new BarEntry(4f,10f));
        barEntries.add(new BarEntry(6f,15f));
        barEntries.add(new BarEntry(8f,50f));
        BarDataSet barDataSet = new BarDataSet(barEntries,"Dates");


        theDates = new ArrayList<>();
        theDates.add("January");
        theDates.add("February");
        theDates.add("March");
        theDates.add("April");
        theDates.add("May");


        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);
        barChart.setScaleEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setTouchEnabled(true);
    }
}
