/* ====================================================================
 * Copyright (c) 2014 Alpha Cephei Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALPHA CEPHEI INC. ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 */

package edu.cmu.pocketsphinx.demo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import static android.widget.Toast.makeText;

public class PocketSphinxActivity extends Activity implements
        RecognitionListener {

    private static DatabaseHandler db;

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";

    private static final String MENU_SEARCH = "menu";

    private static final String SAVE_KEYWORD = "submit";

    private static final String BACK_SEARCH = "back";

    private static final String TIME_SEARCH = "time";
    private static final String LOCATION_SEARCH = "location";
    private static final String EMSTATE_SEARCH = "state";
    private static final String OTHERS_SEARCH = "others";
    private static final String ACTION_SEARCH = "action";

    private static final String GENERIC_SEARCH = "generic";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "hey buddy";

    /* Used to handle permission request */
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private static String time = "", location = "", emstate = "", others = "", action = "";

    private static int switchvalue = 0;
    // 1 - time
    // 2 - location
    // 3 - state
    // 4 - others
    // 5 - action

    private static int dbitem = 1;

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;


    //

    EditText editTime;
    EditText editLocation;
    EditText editInitialEmotion;
    EditText editAfterEmotion;
    EditText editOtherPeople;

    //


    private void menuPage(){
        Intent intent = new Intent(this, MainMenu.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        db = new DatabaseHandler(this);

        // Prepare the data for UI
        captions = new HashMap<String, Integer>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
        captions.put(MENU_SEARCH, R.string.menu_caption);

        captions.put(TIME_SEARCH, R.string.time_caption);
        captions.put(LOCATION_SEARCH, R.string.location_caption);
        captions.put(EMSTATE_SEARCH, R.string.state_caption);
        captions.put(OTHERS_SEARCH, R.string.others_caption);
        captions.put(ACTION_SEARCH, R.string.action_caption);
        captions.put(GENERIC_SEARCH, R.string.input_caption);

        setContentView(R.layout.main);
        ((TextView) findViewById(R.id.caption_text))
                .setText("Preparing Buddy...");

        ((TextView) findViewById(R.id.input_text))
                .setText("Emotional State: " + emstate + "\nAction: " + action);

        editTime = (EditText) findViewById(R.id.editTextTime);
        editLocation = (EditText) findViewById(R.id.editTextLocation);
        editOtherPeople = (EditText) findViewById(R.id.editTextOthers);

        // Check if user has given permission to record audio
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }
        runRecognizerSetup();

        Button stopButton = (Button)findViewById(R.id.button);
        stopButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                recognizer.stop();
                switchSearch(KWS_SEARCH);
            }
        });

        Button submit = (Button) findViewById(R.id.submit_button);
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                time = editTime.getText().toString();
                location = editLocation.getText().toString();
                others = editOtherPeople.getText().toString();

                //menuPage();
            }

        });

    }

    private void runRecognizerSetup() {
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(PocketSphinxActivity.this);
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    ((TextView) findViewById(R.id.caption_text))
                            .setText("Failed to init recognizer " + result);
                } else {
                    switchSearch(KWS_SEARCH);
                }
            }
        }.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runRecognizerSetup();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();
        if (text.equals(KEYPHRASE)) {
            switchSearch(MENU_SEARCH);
            switchvalue = 0;
        }
        else if (text.equals(BACK_SEARCH))
        {
            switchSearch(KWS_SEARCH);
            switchvalue = 0;
        }
        else if (text.equals(TIME_SEARCH))
        {
            ((TextView) findViewById(R.id.caption_text))
                    .setText(R.string.time_caption);
            switchSearch(TIME_SEARCH);
            switchvalue = 1;
        }
        else if (text.equals(LOCATION_SEARCH))
        {
            ((TextView) findViewById(R.id.caption_text))
                    .setText(R.string.location_caption);
            switchSearch(GENERIC_SEARCH);
            switchvalue = 2;
        }
        else if (text.equals(EMSTATE_SEARCH))
        {
            //Set active text field here
            ((TextView) findViewById(R.id.caption_text))
                    .setText(R.string.state_caption);
            switchSearch(GENERIC_SEARCH);
            switchvalue = 3;
        }
        else if (text.equals(OTHERS_SEARCH))
        {
            ((TextView) findViewById(R.id.caption_text))
                    .setText(R.string.others_caption);
            switchSearch(GENERIC_SEARCH);
            switchvalue = 4;
        }
        else if (text.equals(ACTION_SEARCH))
        {
            ((TextView) findViewById(R.id.caption_text))
                    .setText(R.string.action_caption);
            switchSearch(GENERIC_SEARCH);
            switchvalue = 5;
        }
        else if (text.equals(SAVE_KEYWORD))
        {
            ((TextView) findViewById(R.id.result_text)).setText("Saving ...");

            time = editTime.getText().toString();
            location = editLocation.getText().toString();
            others = editOtherPeople.getText().toString();

            HabitCue cue = new HabitCue(0, time, location, emstate, others, action);

            db.addHabitCue(cue);

            ((TextView) findViewById(R.id.result_text)).setText("SAVED!");
            time = "";
            location = "";
            emstate = "";
            others = "";
            action = "";

            time = editTime.getText().toString();
            location = editLocation.getText().toString();
            others = editOtherPeople.getText().toString();
        }
        else if (text.equals("display database"))
        {
            HabitCue cue = db.getHabitCue(dbitem);
            time = cue.getTime();
            location = cue.getLocation();
            emstate = cue.getEmstate();
            others = cue.getOthers();
            action = cue.getAction();
            dbitem += 1;
        }
        else
        {
            ((TextView) findViewById(R.id.result_text)).setText(text);
            switch (switchvalue)
            {
                case 1:
                {
                    time = text;
                    editTime.setText(text);
                    break;
                }
                case 2:
                {
                    location = text;
                    editLocation.setText(text);
                    break;
                }
                case 3:
                {
                    emstate = text;
                    break;
                }
                case 4:
                {
                    others = text;
                    editOtherPeople.setText(text);
                    break;
                }
                case 5:
                {
                    action = text;
                    break;
                }
                default:
                {
                    break;
                }
            }
            ((TextView) findViewById(R.id.input_text))
                    .setText("Emotional State: " + emstate + "\nAction: " + action);
        }

    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        ((TextView) findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
        ((TextView) findViewById(R.id.input_text))
                .setText("Emotional State: " + emstate + "\nAction: " + action);
    }

    @Override
    public void onBeginningOfSpeech() {
    }

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 5000);

        String caption = getResources().getString(captions.get(searchName));
        ((TextView) findViewById(R.id.caption_text)).setText(caption);
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)

                .getRecognizer();
        recognizer.addListener(this);

        /** In your application you might not need to add all those searches.
         * They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

        // Create grammar-based search for selection between demos
        File menuGrammar = new File(assetsDir, "menu.gram");
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        // Create grammar-based search for digit recognition
        File timeGrammar = new File(assetsDir, "time.gram");
        recognizer.addGrammarSearch(TIME_SEARCH, timeGrammar);

        File languageModel_state = new File(assetsDir, "en-70k-0.2-pruned.lm");
        recognizer.addNgramSearch(GENERIC_SEARCH, languageModel_state);
    }

    @Override
    public void onError(Exception error) {
        ((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(MENU_SEARCH);
    }
}
