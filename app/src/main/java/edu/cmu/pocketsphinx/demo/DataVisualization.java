package edu.cmu.pocketsphinx.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.*;
import android.content.*;
import android.view.View;

public class DataVisualization extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_visualization);

        Button graphButton = (Button) findViewById(R.id.button6);
        graphButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Grapher();
            }
        });

    }

    private void Grapher(){
        Intent intent = new Intent(this, ExampleGraph.class);
        startActivity(intent);
    }
}
//figure out how to graph