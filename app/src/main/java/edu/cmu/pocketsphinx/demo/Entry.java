package edu.cmu.pocketsphinx.demo;

/**
 * Created by niravdesai on 2017-01-28.
 */

public class Entry {

    String time;
    String location;
    String emotionalInitial;
    String emotionalAfter;
    String[] people;
    String actionBefore;

    public Entry(String time, String location, String emotionalInitial, String emotionalAfter, String[] people, String actionBefore){
        this.time = time;
        this.location = location;
        this.emotionalAfter = emotionalAfter;
        this.emotionalInitial = emotionalInitial;
        this.people = people;
        this.actionBefore = actionBefore;
    }




}
