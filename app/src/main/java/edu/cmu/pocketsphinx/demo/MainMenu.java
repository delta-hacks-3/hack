package edu.cmu.pocketsphinx.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.*;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button profile = (Button) findViewById(R.id.button2);
        profile.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                userProfile();
            }
        });
        Button dataVisual = (Button) findViewById(R.id.button3);
        dataVisual.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dataVisualization();
            }
        });

        Button datedData = (Button) findViewById(R.id.button4);
        datedData.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                visualizeByDate();
            }
        });

        Button addEntry = (Button) findViewById((R.id.button5));
        addEntry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                newEntry();
            }
        });

    }

    private void newEntry(){
        Intent intent = new Intent(this, PocketSphinxActivity.class);
        startActivity(intent);
    }

    private void dataVisualization(){
        Intent intent = new Intent(this, DataVisualization.class);
        startActivity(intent);
    }

    private void userProfile(){
        Intent intent = new Intent(this, UserProfile.class);
        startActivity(intent);
    }

    private void visualizeByDate(){
        Intent intent = new Intent(this, Calendar.class);
        startActivity(intent);
    }

}
