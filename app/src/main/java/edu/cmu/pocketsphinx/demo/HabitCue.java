package edu.cmu.pocketsphinx.demo;

/**
 * Created by Victor on 2017-01-28.
 */

public class HabitCue {

    String time, location, emstate, others, action;
    int id;
    long datetime;

    public HabitCue(){}

    public HabitCue(int _id, String _time, String _location, String _emstate, String _others, String _action)
    {
        this.id = _id;
        this.time = _time;
        this.location = _location;
        this.emstate = _emstate;
        this.others = _others;
        this.action = _action;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmstate() {
        return emstate;
    }

    public void setEmstate(String emstate) {
        this.emstate = emstate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String Display()
    {
        return "Time: " + time + "\nLocation: " + location + "\nEmotional State: " + emstate + "\nOther People: " + others + "\nAction: " + action;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }
}
